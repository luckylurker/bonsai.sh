# bonsai.sh

<a href="previews/bonsai.png"><img src="https://i.imgur.com/BSvueBS.png" align="right" width=400px></a>

`bonsai.sh` is a bonsai tree generator for the terminal, written entirely in `bash`. It intelligently creates, colors, and positions the tree, and is entirely configurable via CLI options- see [usage](#usage).

There are currently four supported modes: `static` (the default), `live`, `infinite`, and `neofetch`, with more on the way! See [Modes](#modes) for documentation, and [TODO](#todo) for full list of goals.

`bonsai.sh` is a fairly new project, and is always looking for new features or modes- and people to help write them! Feel free to open an issue if you've got an idea.

<br></br>

<br></br>

<br></br>

<br></br>

# Installation

## OS Repositories

### Arch Linux

N.E. Neal was kind enough to package `bonsai.sh` in the AUR as [bonsai.sh-git](https://aur.archlinux.org/packages/bonsai.sh-git/).

Install via AUR helper, e.g. `Yaourt`:

```
yaourt -S bonsai.sh-git
```

## Manual Install

To clone to `~/bin/bonsai.sh`, make executable, and add to available commands:

```bash
git clone https://gitlab.com/jallbrit/bonsai.sh ~/bin/bonsai.sh
chmod +x ~/bin/bonsai.sh/bonsai.sh
ln -s ~/bin/bonsai.sh/bonsai.sh ~/.local/bin/bonsai
```

# Usage

```
Usage: bonsai [OPTIONS]

bonsai.sh is a beautifully random bonsai tree generator.

optional args:
  -l, --live             live mode
  -t, --time TIME        in live mode, minimum time in secs between
                           steps of growth [default: 0.03]
  -i, --infinite         infinite mode
  -w, --wait TIME        in infinite mode, time in secs between
                           tree generation [default: 4]
  -n, --neofetch         neofetch mode
  -m, --message STR      attach message next to the tree
  -T, --termcolors       use terminal colors
  -g, --geometry X,Y     set custom geometry
  -b, --base INT         ascii-art plant base to use, 0 is none
  -c, --leaf STR1,STR2,STR3...   list of strings randomly chosen for leaves
  -M, --multiplier INT   branch multiplier; higher -> more
                           branching (0-20) [default: 5]
  -L, --life INT         life; higher -> more growth (0-200) [default: 28]
  -s, --seed INT         seed random number generator (0-32767)
  -v, --verbose          print information each step of generation
  -h, --help             show help
```

# Modes

## Static

`static` mode is the default- the user only sees the final picture of the tree, and none of the growth along the way.

## Live

`live` mode prints each "step" of growth to the screen so that the user can watch how the tree grows along its lifetime, character by character.

## Infinite

`infinite` mode will keep generating bonsai trees, with a small (configurable) pause in between each, until the user decides to quit with `CTRL+C`. Compatible with any other mode.

## Neofetch

`neofetch` is a [neat program](https://github.com/dylanaraps/neofetch) that displays system information in a pretty format in the terminal. As part of the output, `neofetch` displays colored ascii art, typically of your Linux distribution logo, next to the text. `neofetch` will also take custom ascii art in a [particular format](https://github.com/dylanaraps/neofetch/wiki/Custom-Ascii-art-file-format). `neofetch` mode takes advantage of this feature to output the bonsai tree in perfect format for this purpose. For example:

```bash
bonsai -n -L 20 -g 35,20 > my_bonsai_art.txt
neofetch --ascii my_bonsai_art.txt --ascii_colors 11 3 10 2 0
```

Line 1 specifies smaller life and custom geometry; this is because you want the ascii art for `neofetch` to be much smaller than your terminal size. It also spits the output to a file so that `neofetch` can use it.

In line 2, notice that the `ascii_colors` option was set to `11 3 10 2 0`. `neofetch` mode in `bonsai.sh` is set up so that this sequence will give the optimal colors to the bonsai tree.

# Screenshots

## `bonsai -l -b 2`

<a><img src="https://i.imgur.com/sqmRujD.gif" align=center width=80%></a>

## `bonsai -T -m "$(fortune)"`

<a><img src="https://i.imgur.com/EaHohix.png" align=center width=80%></a>

# TODO

### Mode Requests

* Mode that grows based on percentage throughout minute/hour/day (e.g. in minute mode, when the clock hits 45s, the tree should be 75% done growing and restart when the next minute comes) (credit: u/Esko997)

# Inspiration

 `bonsai.sh` wouldn't be here if it weren't for its *roots*! Originally, this script was simply a port of [this bonsai generator](http://andai.tv/bonsai/), written in JS, but has since evolved to include multiple CLI options and significantly improved tree generation.
